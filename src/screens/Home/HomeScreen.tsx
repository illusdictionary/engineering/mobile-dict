import { ScrollView, StyleSheet, Image, View, Text } from "react-native";
import React, { useEffect } from 'react';
import axios from 'axios';
import { REACT_APP_ENDPOINT, REACT_APP_TOKEN } from "@env";

interface IWords {
  id: string
  title: string
  definition: string
  illustration: string
};

const baseClient = axios.create({
  baseURL: REACT_APP_ENDPOINT as string
});

export default function HomeScreen() {
  const [words, setWords] = React.useState([]);
  const [error, setError] = React.useState();
  const [isLoading, setLoading] = React.useState(false);

  useEffect(() => {
    setLoading(true)
    try {
      const fetchWords = async () => {
        const response = await baseClient.get('/words/', {
          headers: {
            Authorization: `Token ${REACT_APP_TOKEN}`
          }
        })
        setWords(response.data);
      }
      fetchWords();
      setLoading(false);
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    } catch (error: any) {
      if (error.response) {
        setError(error)
      } else if (error.request) {
        setError(error)
      } else {
        setError(error)
      }
    }
  }, [])

  if (isLoading) {
    return <div>Loading...</div>
  }

  if (error) {
    const { message } = error
    return <div>An error occured: {message}</div>
  }

  return (
    <ScrollView>
        <View style={styles.container}>
          {
            words.map((word: IWords) => {
              return (
                <View key={word.id} style={styles.wordContainer}>
                  <Image
                    style={{width: 120, height: 120}}
                    source={{
                      uri: word.illustration
                      }} />
                  <Text style={{fontWeight: 'bold'}}>{word.title}</Text>
                </View>
              )
            })
          }
        </View>
      </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    display: 'flex',
    flexDirection: 'row',
    marginTop: 15,
    flexWrap: 'wrap',
    marginLeft: 15,
    marginRight: 15,
  },
  wordContainer: {
    marginRight: 2,
    alignItems: 'center'
  },
  number: {
    fontSize: 23
  },
  chart: {
    backgroundColor: "#ffffff",
    width: "95%",
    marginTop: "5%",
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  list: {
    backgroundColor: "#ffffff",
    width: "95%",
    marginTop: "5%",
    flex: 1,
    alignItems: "center",
  },
});
