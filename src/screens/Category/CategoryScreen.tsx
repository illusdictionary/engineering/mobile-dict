import { StyleSheet, Text, View } from "react-native";

import React from "react";

export default function CategoryScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Animals</Text>
      <Text style={styles.title}>Cars</Text>
      <Text style={styles.title}>Cooking</Text>
      <Text style={styles.title}>Furniture</Text>
      <Text style={styles.title}>Plants</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 20,
  },
});
