declare module '@env' {
  export const REACT_APP_ENDPOINT: string;
  export const REACT_APP_TOKEN: string;
}