export type RootStackParamList = {
  Root: undefined;
};

export type RootTabParamList = {
  homeTab: undefined;
  categoryTab : undefined;
};