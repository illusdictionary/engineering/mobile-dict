import 'react-native-gesture-handler';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from "@react-navigation/stack"
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { RootStackParamList, RootTabParamList } from '../types'
import { Octicons, Entypo } from '@expo/vector-icons'
import Colors from '../src/constants/Colors'
import HomeScreen from '../src/screens/Home/HomeScreen'
import CategoryScreen from '../src/screens/Category/CategoryScreen'
import React from 'react'

export default function Navigation () {
  return (
    <NavigationContainer>
      <RootNavigator />
    </NavigationContainer>
  )
}

/**
 * A root stack navigator is often used for displaying modals on top of all other content.
 * https://reactnavigation.org/docs/modal
 */

const Stack = createNativeStackNavigator<RootStackParamList>();

const RootNavigator = (() => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Root" component={BottomTabNavigator} options={{ headerShown: false }} />
    </Stack.Navigator>
  )
})

/**
 * A bottom tab navigator displays tab buttons on the bottom of the display to switch screens.
 * https://reactnavigation.org/docs/bottom-tab-navigator
 */
const BottomTab = createBottomTabNavigator<RootTabParamList>();

function BottomTabNavigator () {
  return (
    <BottomTab.Navigator
      initialRouteName="homeTab"
      screenOptions={{
        tabBarItemStyle: {
          height: 52,
        },
        headerShown: false
      }}
    >
      <BottomTab.Screen
        name="homeTab"
        component={HomeNavigator}
        options={{
          title: 'Home',
          tabBarIcon: ({ focused }) => <Octicons name="home" size={23} />
        }}
      />
      <BottomTab.Screen
        name="categoryTab"
        component={CategoryNavigator}
        options={{
          title: 'Category',
          tabBarIcon: ({ focused }) => <Entypo name="menu" size={23} />
        }}
      />
    </BottomTab.Navigator>
  )
}

// Each tab has its own navigation stack, you can read more about this pattern here:
// https://reactnavigation.org/docs/tab-based-navigation#a-stack-navigator-for-each-tab
const HomeStack = createStackNavigator();

function HomeNavigator() {
  return (
    <HomeStack.Navigator>
      <HomeStack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          headerTitle: "Home",
          headerStyle: {backgroundColor: '#497bd0',},
          headerTintColor: '#fff'
        }}
      />
    </HomeStack.Navigator>
  );
}

const CategoryStack = createStackNavigator();

function CategoryNavigator() {
  return (
    <CategoryStack.Navigator>
      <CategoryStack.Screen
        name="CategoryScreen"
        component={CategoryScreen}
        options={{
          headerTitle: "Category",
          headerStyle: {backgroundColor: '#497bd0',},
          headerTintColor: '#fff'
        }}
      />
    </CategoryStack.Navigator>
  );
}
